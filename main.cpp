#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/text.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main( int argc, char** argv )
{
    Mat image;
    Mat step1, step2, step3;
    String imageName =("test_marker.jpg");
    image = imread(imageName, IMREAD_COLOR );
    if( image.empty() )
    {
        printf("Error opening image: %s\n", imageName.c_str());
        return EXIT_FAILURE;
    }

    // перевожу в HSV и выделяю канал H
    cvtColor(image, step1, COLOR_BGR2HSV);
    Mat channels1[3];
    split(step1, channels1);
    step1=channels1[1];

    // ищу красную область, к ней относится нужный красный прямоугольник
    inRange(step1,Scalar(1,36,30),Scalar(75,255,255),step1);

    // использую операцию открытия, чтобы убрать маленькие области, содержащие красный
    erode(step1,step1,getStructuringElement(MORPH_ELLIPSE,Size(3,3))); // сужение
    dilate(step1,step1,getStructuringElement(MORPH_ELLIPSE,Size(88,88))); // расширение

    //выделяю границы
    Canny(step1,step1,30,90);

    //ищу контуры
    vector<Point> contour1;
    vector<vector<Point>> contours1;
    findContours(step1,contours1,RETR_TREE,CHAIN_APPROX_SIMPLE);

    // ищу контур с максимальной площадью. Визуально это красный прямоугольник
    contour1=contours1[0];
    for(int i=0;i<contours1.size();i++)
    {
        if(fabs(contourArea(contours1[i]))>fabs(contourArea(contour1)))
                contour1=contours1[i];
    }

    // выделяю область с красным прямоугольником из изображения
    Rect rectang1 = boundingRect(contour1);
    step2 = image(rectang1);

    // перевожу в черно-белый
    cvtColor(step2, step3, COLOR_BGR2GRAY);

    // использую операцию открытия, чтобы убрать текст
    erode(step3, step3,getStructuringElement(MORPH_ELLIPSE,Size(11,11)));
    dilate(step3,step3,getStructuringElement(MORPH_ELLIPSE,Size(22,22)));

    // использую операцию закрытия, чтобы вернуть черным прямоугольникам исходный размер
    dilate(step3,step3,getStructuringElement(MORPH_ELLIPSE,Size(26,26)));
    erode(step3, step3,getStructuringElement(MORPH_ELLIPSE,Size(29,29)));

    //ищу границы
    Canny(step3,step3,30,90);

    //ищу контуры, осталось только три черных прямоугольника
    vector<vector<Point>> contours2;
    findContours(step3,contours2,RETR_EXTERNAL,CHAIN_APPROX_SIMPLE);

    // выделяю области с черными прямоугольниками
    Mat found1, found2, found3;
    Rect rectang2 = boundingRect(contours2[2]);
    found1 = step2(rectang2);
    rectang2 = boundingRect(contours2[1]);
    found2 = step2(rectang2);
    rectang2 = boundingRect(contours2[0]);
    found3 = step2(rectang2);

    // распознаю текст
    Ptr<text::BaseOCR> ocr = text::OCRTesseract::create(nullptr, "rus");
    string text1;
    ocr->run(found1, text1);
    cout << "код: " << text1 << endl;
    string text2;
    ocr->run(found2, text2);
    cout << "ряд: " << text2 << endl;
    string text3;
    ocr->run(found3, text3);
    cout << "место: " << text3 << endl;

    // вывожу искомые части картинки на экран
    imshow("код",found1);
    imshow("ряд",found2);
    imshow("место",found3);

    char key = (char)waitKey(0);
    return EXIT_SUCCESS;
}